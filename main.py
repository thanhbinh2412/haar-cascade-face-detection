import tkinter
import sys
# import src file
sys.path.insert(0, './src')

# import
from run import App


def main():
    # WindowCamera(tkinter.Tk(), "Haar Cascade - Face detection")
    App("Haar Cascade - Face detection")
    # video_capture()


if __name__ == "__main__":
    main()