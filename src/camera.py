# import
import cv2
import os
import sqlite3


class MyVideoCapture():

    def __init__(self, video_source=0):
        # open video source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

        # self.face_detector1 = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        haarcascades_path = os.path.dirname(
            cv2.__file__) + "\data\haarcascade_frontalface_default.xml"
        self.face_detector1 = cv2.CascadeClassifier(haarcascades_path)

        haarcascades_path_eye = os.path.dirname(
            cv2.__file__) + "\data\haarcascade_eye.xml"
        self.eye_dectector1 = cv2.CascadeClassifier(haarcascades_path_eye)

        self.rec=cv2.face.LBPHFaceRecognizer_create();
        pathyml = os.path.abspath('') + "\\recognizer\\trainningData.yml"
        print(pathyml)
        self.rec.read(pathyml)
        self.id=0

    def get_frame(self):
        fontface = cv2.FONT_HERSHEY_SIMPLEX
        fontscale = 1
        fontcolor = (203,23,252)
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # =====================================
                # Face Detection
                # =====================================

                gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
                faces=self.face_detector1.detectMultiScale(gray,1.3,5)

                for (x, y, w, h) in faces:
                    cv2.rectangle(frame,
                                  pt1=(x, y),
                                  pt2=(x + w, y + h),
                                  color=(255, 0, 0),
                                  thickness=3)
                    id,conf=self.rec.predict(gray[y:y+h,x:x+w])
                    profile=self.getProfile(id)

                    #set text to window
                    if(profile!=None):
                        #cv2.PutText(cv2.fromarray(img),str(id),(x+y+h),font,(0,0,255),2);
                        cv2.putText(frame, "Name: " + str(profile[1]), (x,y+h+30), fontface, fontscale, fontcolor ,2)
                        # cv2.putText(frame, "Age: " + str(profile[2]), (x,y+h+60), fontface, fontscale, fontcolor ,2)
                        # cv2.putText(frame, "Gender: " + str(profile[3]), (x,y+h+90), fontface, fontscale, fontcolor ,2)

                # =====================================
                # End Face Detection
                # =====================================

                # Return a boolean success flag and the current frame converted to BGR
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (False, None)
    
    def getProfile(self, id):
        path = 'FaceBase.db'
        conn=sqlite3.connect(path)
        cmd="SELECT * FROM User WHERE ID="+str(id)
        cursor=conn.execute(cmd)
        profile=None
        for row in cursor:
            profile=row
        conn.close()
        return profile

    # Release the video source when object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()