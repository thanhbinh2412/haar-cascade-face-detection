import tkinter as tk
from tkinter import filedialog
from tkinter.filedialog import askopenfile
from PIL import Image, ImageTk, ImageOps
from tkinter import messagebox
import cv2
import os
import numpy

# ----------------------------------
import src.run


class BySelect:

    def __init__(self, window_title):

        self.window = tk.Tk()
        self.window.title(window_title)
        self.window.geometry("700x600")

        # self.face_detector1 = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        haarcascades_path = os.path.dirname(
            cv2.__file__) + "\data\haarcascade_frontalface_default.xml"
        self.face_detector1 = cv2.CascadeClassifier(haarcascades_path)

        haarcascades_path_eye = os.path.dirname(
            cv2.__file__) + "\data\haarcascade_eye.xml"
        self.eye_dectector1 = cv2.CascadeClassifier(haarcascades_path_eye)

        self.my_font1 = ('times', 18, 'bold')

        l1 = tk.Label(self.window,
                      text='Select Image',
                      width=30,
                      font=self.my_font1)

        l1.grid(row=1, column=1)
        b1 = tk.Button(self.window,
                       text='Upload File',
                       width=20,
                       command=lambda: self.upload_file())
        b1.grid(row=2, column=1)

        self.window.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.window.mainloop()

    def upload_file(self):
        global img
        f_types = [('Jpg Files', '*.jpg')]
        filename = filedialog.askopenfilename(filetypes=f_types)
        img = ImageTk.PhotoImage(file=filename)
        b2 =tk.Button(self.window,image=img) # using Button 
        b2.grid(row=3,column=1)
        # global img
        # f_types = [('Jpg Files', '*.jpg')]
        # filename = filedialog.askopenfilename(filetypes=f_types)
        # #Load the image
        # img = cv2.imread(filename)
        # print(img)
        # # =====================================
        # # Face Detection
        # # =====================================
        # # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        # # self.faces = self.face_detector1.detectMultiScale(gray, 1.1, 4)

        # # for (x, y, w, h) in self.faces:
        # #     cv2.rectangle(img,
        # #                   pt1=(x, y),
        # #                   pt2=(x + w, y + h),
        # #                   color=(255, 0, 0),
        # #                   thickness=3)
        # #     roi_gray = gray[y:y + h, x:x + w]
        # #     roi_color = img[y:y + h, x:x + w]

        # #     eyes = self.eye_dectector1.detectMultiScale(roi_gray)

        # #     for (ex, ey, ew, eh) in eyes:
        # #         cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh),
        # #                       (0, 255, 0), 5)

        # # Convert the Image object into a TkPhoto object
        # im = Image.fromarray(img)
        # imgtk = ImageTk.PhotoImage(image=im)

        # # =====================================
        # # End Face Detection
        # # =====================================
        # b2 = tk.Button(self.window, image=imgtk)  # using Button
        # b2.grid(row=3, column=1)

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.window.destroy()
            src.run.App("Haar Cascade - Face detection")