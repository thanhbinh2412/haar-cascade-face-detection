import tkinter as tk
from tkinter import Toplevel
import PIL.Image, PIL.ImageTk
import sqlite3
from tkinter import messagebox

# ----------------------------------
from camera import MyVideoCapture
import src.run


class WindowCamera():

    def __init__(self, window_title, video_source=0):

        self.window = Toplevel()
        self.window.title(window_title)
        self.video_source = video_source

        self.window.geometry("700x600")

        # open video source
        self.vid = MyVideoCapture(video_source)

        # column configure
        self.window.columnconfigure(0, weight=1)

        self.canvas = tk.Canvas(self.window,
                                width=self.vid.width,
                                height=self.vid.height)
        self.canvas.grid(column=0, row=0)

        self.delay = 15
        self.update()
        self.window.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.window.mainloop()

    def update(self):
        ret, frame = self.vid.get_frame()
        if ret:
            self.photo = PIL.ImageTk.PhotoImage(
                image=PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        else:
            print('dsds')

        self.window.after(self.delay, self.update)

    def insertOrUpdateData(self, Id, Name):
        self.conn = sqlite3.connect("../data/FaceBase.db")
        cmd = "SELECT * FROM User WHERE ID=" + str(Id)
        cursor = self.conn.execute(cmd)
        isRecordExist = 0
        for row in cursor:
            isRecordExist = 1
        if (isRecordExist == 1):
            cmd = "UPDATE User SET Name=" + str(Name) + "WHERE ID=" + str(Id)
        else:
            cmd = "INSERT INTO User(Id,Name) Values(" + str(Id) + "," + str(
                Name) + ")"
        self.conn.execute(cmd)
        self.conn.commit()
        self.conn.close()

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.window.destroy()
            src.run.App("Haar Cascade - Face detection")
