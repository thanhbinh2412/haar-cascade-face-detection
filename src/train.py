import tkinter as tk
from tkinter import Toplevel
import PIL.Image, PIL.ImageTk
import sqlite3
from tkinter import messagebox
import tkinter.font as tkFont
import os
import cv2
from PIL import Image
import numpy as np

# ----------------------------------
from camera_train import MyCamTrain
import src.run


class TrainDataSet():

    isactive = False

    def __init__(self, window_title, video_source=0):

        self.window = Toplevel()
        self.window.title(window_title)
        self.video_source = video_source

        self.window.geometry("900x500")

        # open video source
        self.vid = MyCamTrain(video_source)

        # column configure
        self.window.rowconfigure(0, weight=1)
        self.window.rowconfigure(1, weight=6)
        self.window.rowconfigure(2, weight=1)

        # Label Screen
        self.lable = tk.Label(self.window, text="Face Detection")
        self.lable.place(x=450, y=10)

        # Text box - ID
        ft = tkFont.Font(family='Times',size=11)
        self.lbl_id = tk.Label(self.window, text="ID", font=ft, justify="left")
        self.lbl_id.place(x=650, y=40, width=70)

        self.input_id = tk.Text(self.window,
                                    height = 1,
                                    width = 20)
        self.input_id.place(x=720,y=40)

        # Text box - NAME
        ft = tkFont.Font(family='Times',size=11)
        self.lbl_name = tk.Label(self.window, text="Full name", font=ft, justify="left")
        self.lbl_name.place(x=650, y=70, width=70)

        self.input_name = tk.Text(self.window,
                                    height = 1,
                                    width = 20)
        self.input_name.place(x=720,y=70)

        # Button Start
        self.btn = tk.Button(self.window, text="Start", command=self.start)
        self.btn.place(x=720,y=100)

        # Button Training
        self.btn_train = tk.Button(self.window, text="Train", command=self.train)
        self.btn_train.place(x=820,y=100)

        # label file name
        self.lbl_filename = tk.Label(self.window, text="*Filename:")
        self.lbl_filename.place(x=650,y=150)

        print(self.vid.width)
        print(self.vid.height)
        
        self.canvas = tk.Canvas(self.window,
                                width=self.vid.width,
                                height=self.vid.height)
        self.canvas.place(x=0,y=40,width=self.vid.width,height=self.vid.height)

        self.delay = 15
        self.update()
            
        self.window.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.window.mainloop()

    def train(self):
        self.training()

    def start(self):
        self.next = False
        self.insertOrUpdateData(Id=str(self.input_id.get(1.0, "end-1c")), Name=str(self.input_name.get(1.0, "end-1c")))

        if self.next:
            self.vid.cap = True
            self.vid.name = str(self.input_name.get(1.0, "end-1c"))
            self.vid.id = str(self.input_id.get(1.0, "end-1c"))

            filename_tmp = 'dataSet/' + str(self.input_name.get(1.0, "end-1c")) + '.' + str(self.input_id.get(1.0, "end-1c")) + '.[1->20].jpg'
            self.lbl_filename.config(text="*Filename:"+filename_tmp)

    def update(self):
        ret, frame = self.vid.get_frame()
        if ret:
            self.photo = PIL.ImageTk.PhotoImage(
                image=PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image=self.photo, anchor=tk.NW)
        else:
            print('dsds')

        self.window.after(self.delay, self.update)

    def insertOrUpdateData(self, Id, Name):
        # path = os.path.dirname(cv2.__file__) + "\data\FaceBase.db"
        path = 'FaceBase.db'
        self.conn = sqlite3.connect(path)
        cmd = "SELECT * FROM User WHERE ID=" + str(Id)
        cursor=self.conn.execute(cmd)
        isRecordExist=0
        for row in cursor:
            isRecordExist=1
        if(isRecordExist==1):
            # cmd="UPDATE User SET Name='"+str(Name)+"' WHERE ID="+str(Id)
            messagebox.showwarning('Cảnh báo', 'Đã tồn tại Id người dùng')
            self.next = False
        else:
            cmd="INSERT INTO User(ID,Name) Values("+str(Id)+",'"+str(Name)+"')"
            self.conn.execute(cmd)
            self.conn.commit()
            self.next = True

        self.conn.close()

    def checkIdExist(self, Id):
        self.conn = sqlite3.connect("../data/FaceBase.db")
        cmd = "SELECT * FROM User WHERE ID=" + str(Id)
        cursor = self.conn.execute(cmd)
        isRecordExist = 0
        for row in cursor:
            isRecordExist = 1
        self.conn.execute(cmd)
        self.conn.commit()
        self.conn.close()

    def on_closing(self):
        if messagebox.askokcancel("Quit", "Do you want to quit?"):
            self.window.destroy()
            src.run.App("Haar Cascade - Face detection")

    def training(self):
        recognizer = cv2.face.LBPHFaceRecognizer_create()
        path='dataSet'

        #get the path of all the files in the folder
        imagePaths=[os.path.join(path,f) for f in os.listdir(path)] 
        faces=[]
        IDs=[]
        for imagePath in imagePaths:
            faceImg=Image.open(imagePath).convert('L');
            faceNp=np.array(faceImg,'uint8')
            #split to get ID of the image
            ID=int(os.path.split(imagePath)[-1].split('.')[1])
            faces.append(faceNp)
            print(ID)
            IDs.append(ID)
            cv2.imshow("traning",faceNp)
            cv2.waitKey(10)
        
        recognizer.train(faces,np.array(IDs))
        recognizer.save('recognizer/trainningData.yml')
        cv2.destroyAllWindows()