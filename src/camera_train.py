# import
import cv2
import os
from tkinter import messagebox


class MyCamTrain():

    def __init__(self, video_source=0):
        self.cap = False
        self.name = ""
        self.id = ""
        # open video source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

        # self.face_detector1 = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        haarcascades_path = os.path.dirname(
            cv2.__file__) + "\data\haarcascade_frontalface_default.xml"
        self.face_detector1 = cv2.CascadeClassifier(haarcascades_path)

        haarcascades_path_eye = os.path.dirname(
            cv2.__file__) + "\data\haarcascade_eye.xml"
        self.eye_dectector1 = cv2.CascadeClassifier(haarcascades_path_eye)

        self.sampleNum = 0

    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                # =====================================
                # Face Detection
                # =====================================

                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                self.faces = self.face_detector1.detectMultiScale(gray, 1.1, 4)

                for (x, y, w, h) in self.faces:
                    cv2.rectangle(frame,
                                  pt1=(x, y),
                                  pt2=(x + w, y + h),
                                  color=(255, 0, 0),
                                  thickness=3)

                    if self.cap:
                        #incrementing sample number
                        self.sampleNum = self.sampleNum + 1
                        #saving the captured face in the dataset folder
                        if self.sampleNum <= 20:
                            cv2.imwrite(
                                "dataSet/" + str(self.name) + '.' + str(self.id) + '.' + str(self.sampleNum) +
                                ".jpg", gray[y:y + h, x:x + w])
                        if self.sampleNum == 21:
                            messagebox.showinfo('Thành công', 'Tạo khuôn mặt thành công')

                # =====================================
                # End Face Detection
                # =====================================

                # Return a boolean success flag and the current frame converted to BGR
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (False, None)
    
    
    # Release the video source when object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

    def releaseCam(self):
        if self.vid.isOpened():
            self.vid.release()