# import
import cv2
import tkinter as tk
import PIL.Image, PIL.ImageTk
import sys
from tkinter import ttk, messagebox
import tkinter.font as tkFont

import src.by_camera
import src.train
import src.by_select


class App():

    def __init__(self, window_title, video_source=0):

        self.window = tk.Tk()
        self.window.title(window_title)
        self.video_source = video_source

        self.window.geometry("530x300")

        #Title
        Title_main=tk.Label(self.window)
        ft = tkFont.Font(family='Times',size=24)
        Title_main["font"] = ft
        Title_main["fg"] = "#333333"
        Title_main["justify"] = "center"
        Title_main["text"] = "Face Detection - Haar Cascade"
        Title_main.place(x=0,y=20,width=530,height=50)

        # Button
        self.type = 1 # 1: by camera | 2: by folder

        # Row 1
        GButton_bycamera=tk.Button(self.window)
        GButton_bycamera["bg"] = "#f0f0f0"
        ft = tkFont.Font(family='Times',size=10)
        GButton_bycamera["font"] = ft
        GButton_bycamera["fg"] = "#000000"
        GButton_bycamera["justify"] = "center"
        GButton_bycamera["text"] = "Máy ảnh"
        GButton_bycamera.place(x=90,y=140,width=150,height=40)
        GButton_bycamera["command"] = lambda types = 1 : self.GCheckBox(type=types)

        GButton_byfolder=tk.Button(self.window)
        GButton_byfolder["bg"] = "#f0f0f0"
        ft = tkFont.Font(family='Times',size=10)
        GButton_byfolder["font"] = ft
        GButton_byfolder["fg"] = "#000000"
        GButton_byfolder["justify"] = "center"
        GButton_byfolder["text"] = "Tải ảnh"
        GButton_byfolder.place(x=280,y=140,width=150,height=40)
        GButton_byfolder["command"] = lambda types = 2 : self.GCheckBox(type=types)

        # Row 2
        GButton_train=tk.Button(self.window)
        GButton_train["bg"] = "#f0f0f0"
        ft = tkFont.Font(family='Times',size=10)
        GButton_train["font"] = ft
        GButton_train["fg"] = "#000000"
        GButton_train["justify"] = "center"
        GButton_train["text"] = "Training DataSet"
        GButton_train.place(x=90,y=210,width=150,height=40)
        GButton_train["command"] = lambda types = 3 : self.GCheckBox(type=types)

        GButton_cancel=tk.Button(self.window)
        GButton_cancel["activebackground"] = "#ab303c"
        GButton_cancel["activeforeground"] = "#ab303c"
        GButton_cancel["bg"] = "#dc3545"
        ft = tkFont.Font(family='Times',size=10)
        GButton_cancel["font"] = ft
        GButton_cancel["fg"] = "#fff"
        GButton_cancel["justify"] = "center"
        GButton_cancel["text"] = "Cancel"
        GButton_cancel.place(x=280,y=210,width=150,height=40)
        GButton_cancel["command"] = self.cancelWindow

        self.window.mainloop()


    # ====================
    # ==== Function ======
    # ====================

    def GCheckBox(self, type):
        self.type = type
        print("type: ", type)
        match type:
            case 1:
                print("Detection by Camera")
                self.window.destroy()
                src.by_camera.WindowCamera('Detection by Camera')
            case 2:
                print("Detection by Image")
                self.window.destroy()
                src.by_select.BySelect('Detection by Image')
            case 3:
                print("Training DataSet")
                self.window.destroy()
                src.train.TrainDataSet('Training DataSet')

    def cancelWindow(self):
        print("cancel")
        if messagebox.askokcancel("Face Detection - Haar Cascade", "Do you want to quit?"):
            self.window.destroy()
    
    def ok(self):
        print("ok")

